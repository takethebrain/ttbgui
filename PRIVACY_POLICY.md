# Privacy Policy

We, 'Smess' and/or 'Take the Brain' take your privacy seriously. To better protect your privacy we provide this privacy policy notice.


## Collection of Routine Information

This app does not track ANY information about their users. 


## Cookies

This app does NOT use ANY cookies in any form.


## Advertisement and Other Third Parties

This app does NOT use any Advertisement nor any third pary libraries.


## Links to Third Party Websites

We have included links on this app for your use and reference. We are not responsible for the privacy policies on these websites. You should be aware that the privacy policies of these websites may differ from our own.


## Security

As we do not track any information about users there are no security measures in place in the app for projection of such.


## Changes To This Privacy Policy

This Privacy Policy is effective as of 2022-06-22 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.

We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Notification of changes will not be made through the app.


## Contact Information

For any questions or concerns regarding the privacy policy, please send us an email to takethebrain@gmail.com.
