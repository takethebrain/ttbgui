package nl.kresict.ttbgui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;
import nl.kresict.games.ttb.ai.AIPlayer;
import nl.kresict.games.ttb.domain.GameType;
import nl.kresict.games.ttb.domain.HumanPlayer;
import nl.kresict.games.ttb.domain.IPlayer;
import nl.kresict.games.ttb.domain.TtbGameBoard;
import nl.kresict.games.ttb.services.TtbBoardService;

public class GameActivity extends Activity implements View.OnLongClickListener, View.OnClickListener,
        View.OnTouchListener {

    DragController mDragController;
    DragLayer mDragLayer;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_layout);

        GridView gridView = (GridView) findViewById(R.id.grid_view);
        
        Intent intent = getIntent();
        String gameType = intent.getStringExtra(MenuActivity.GAME_TYPE);
        
        IPlayer player1;
        IPlayer player2;
        GameType type;
        
        if (gameType.equalsIgnoreCase("aiEasy")) {
            player1=new HumanPlayer("blue");
            player2=new AIPlayer("red");
            type = GameType.EASY;
        }
        else if (gameType.equalsIgnoreCase("aiHard")) {
            player1=new HumanPlayer("blue");
            player2=new AIPlayer("red");
            type = GameType.HARD;
        }
        else {
            player1=new HumanPlayer("blue");
            player2=new HumanPlayer("red");
            type = null;
        }

        TtbGameBoard board = TtbBoardService.initBoard(player1, player2, type);

        // Instance of ImageAdapter Class
        gridView.setAdapter(new ImageCellAdapter(this, board));

        mDragController = new DragController(this, board);
        mDragLayer = (DragLayer) findViewById(R.id.drag_layer);
        mDragLayer.setDragController(mDragController);
        mDragLayer.setGridView(gridView);
        mDragController.setDragListener(mDragLayer);

        // Give the user a little guidance.
        Toast.makeText(getApplicationContext(),
                getResources().getString(R.string.instructions),
                Toast.LENGTH_LONG).show();
    }

    public boolean onLongClick(View v) {
        return startDrag(v);
    }

    public void onClick(View v) {
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startDrag(v);
                break;
            default:
                break;
        }
        return true;
    }

    public boolean startDrag(View v) {
        IDragSource dragSource = (IDragSource) v;

        // We are starting a drag. Let the DragController handle it.
        mDragController.startDrag(v, dragSource, dragSource, DragController.DRAG_ACTION_MOVE);

        return true;
    }
}
