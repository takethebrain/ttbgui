/*
 * This is a modified version of a class from the Android Open Source Project. 
 * The original copyright and license information follows.
 * 
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.kresict.ttbgui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.Toast;
import nl.kresict.games.ttb.ai.AIPlayer;
import nl.kresict.games.ttb.domain.Move;
import nl.kresict.games.ttb.domain.Piece;
import nl.kresict.games.ttb.domain.TtbGameBoard;
import nl.kresict.games.ttb.services.TtbMoveDecisionHelper;
import nl.kresict.games.ttb.services.ex.EndOfGameException;
import nl.kresict.games.ttb.services.ex.HitException;
import nl.kresict.games.ttb.services.ex.IllegalMoveException;
import nl.kresict.games.ttb.services.ex.NinnyReplaceException;

/**
 * A ViewGroup that supports dragging within it. Dragging starts in an object
 * that implements the IDragSource interface and ends in an object that
 * implements the IDropTarget interface.
 *
 * <p> This class used DragLayer in the Android Launcher activity as a model. It
 * is a bit different in several respects: (1) it supports dragging to a grid
 * view and trash area; (2) it dynamically adds drop targets when a drag-drop
 * sequence begins. The child views of the GridView are assumed to implement the
 * IDropTarget interface.
 */
public class DragLayer extends FrameLayout
        implements DragController.DragListener {

    DragController mDragController;
    GridView mGridView;

    /**
     * Used to create a new DragLayer from XML.
     *
     * @param context The application's context.
     * @param attrs The attribtues set containing the Workspace's customization
     * values.
     */
    public DragLayer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setDragController(DragController controller) {
        mDragController = controller;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return mDragController.dispatchKeyEvent(event) || super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mDragController.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return mDragController.onTouchEvent(ev);
    }

    @Override
    public boolean dispatchUnhandledMove(View focused, int direction) {
        return mDragController.dispatchUnhandledMove(focused, direction);
    }

    /**
     * Get the value of the GridView property.
     *
     * @return GridView
     */
    public GridView getGridView() {
        //if (mGridView == null) {}
        return mGridView;
    } // end getGridView

    /**
     * Set the value of the GridView property.
     *
     * @param newValue GridView
     */
    public void setGridView(GridView newValue) {
        mGridView = newValue;
    } // end setGridView
/* end Property GridView */

    /**
     */
// DragListener Interface Methods
    /**
     * A drag has begun.
     *
     * @param source An object representing where the drag originated
     * @param info The data associated with the object that is being dragged
     * @param dragAction The drag action: either
     * {@link DragController#DRAG_ACTION_MOVE} or
     * {@link DragController#DRAG_ACTION_COPY}
     */
    public void onDragStart(IDragSource source, Object info, int dragAction) {
        // We are starting a drag. 
        // Build up a list of DropTargets from the child views of the GridView.
        // Tell the drag controller about them.

        if (mGridView != null) {
            int numVisibleChildren = mGridView.getChildCount();
            for (int i = 0; i < numVisibleChildren; i++) {
                IDropTarget view = (IDropTarget) mGridView.getChildAt(i);
                mDragController.addDropTarget(view);
            }
        }

    }

    /**
     * A drag-drop operation has eneded.
     */
    public void onDragEnd() {
        mDragController.removeAllDropTargets();
        turnTables();
    }

    private void turnTables() {
        TtbGameBoard board = mDragController.getBoard();
        if (!mDragController.getGameEnded()) {
            if (board.getTurn().getClass()==AIPlayer.class) {
                AIPlayer p = (AIPlayer)board.getTurn();
                Move move = p.getMove(board);
                try {
                    TtbMoveDecisionHelper.isValidMove(board, move);
                } catch (IllegalMoveException ex) {
                    //do nothing. shouldn't occur
                } catch (EndOfGameException ex) {
                    //set end of game
                    Toast.makeText(mDragController.getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                    this.mDragController.setGameEnded(true);

                } catch (HitException ex) {
                    //do nothing. should be handeled
                    Toast.makeText(mDragController.getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (NinnyReplaceException ex) {
                    //replace field on board
                    board.replacePiece(move.getFrom(), Piece.NUMSCULL);
                    IDragSource source = findDragSource(move.getFrom().getX(), move.getFrom().getY());
                    source.changePiece();
                    Toast.makeText(mDragController.getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
                board.movePiece(move);

                IDragSource source = findDragSource(move.getFrom().getX(), move.getFrom().getY());
                IDropTarget target = findDropTarget(move.getTo().getX(), move.getTo().getY());
                target.onAiDrop(source);
                source.onAiDrag();
            }
            else {
                //do nothing
            }
        }
        else {
            //do nothing; game has ended
        }
    }

    private IDropTarget findDropTarget(int x, int y) {
        if (this.getGridView() != null) {
            int numVisibleChildren = this.getGridView().getChildCount();
            for (int i = 0; i < numVisibleChildren; i++) {
                IDropTarget view = (IDropTarget) this.getGridView().getChildAt(i);
                if (view.getField().getX()==x && view.getField().getY()==y) {
                    return view;
                }
            }
        }
        return null;
    }
    
    private IDragSource findDragSource(int x, int y) {
        if (this.getGridView() != null) {
            int numVisibleChildren = this.getGridView().getChildCount();
            for (int i = 0; i < numVisibleChildren; i++) {
                IDragSource view = (IDragSource) this.getGridView().getChildAt(i);
                if (view.getField().getX()==x && view.getField().getY()==y) {
                    return view;
                }
            }
        }
        return null;
    }

} // end class
