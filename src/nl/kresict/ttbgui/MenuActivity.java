package nl.kresict.ttbgui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class MenuActivity extends Activity {
    
    public final static String GAME_TYPE = "nl.kresict.ttbgui.GAME_TYPE";
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);
         
    }
    
    public void startOneOnOne(View view) {
        startGame("oneOnOne");
    }
    
    public void startAiEasy(View view) {
        startGame("aiEasy");
    }
    
    public void startAiHard(View view) {
        startGame("aiHard");
    }
    public void startRules(View view) {
        Intent intent = new Intent(this, RulesActivity.class);
        startActivity(intent);
    }
    
    public void startGame(String gameType) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(GAME_TYPE, gameType);
        startActivity(intent);
    }
    
}
