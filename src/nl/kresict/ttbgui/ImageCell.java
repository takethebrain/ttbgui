package nl.kresict.ttbgui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import nl.kresict.games.ttb.domain.TtbField;

/**
 * This subclass of ImageView is used to display an image on an GridView. An
 * ImageCell knows which cell on the grid it is showing and which grid it is
 * attached to Cell numbers are from 0 to NumCells-1. It also knows if it is
 * empty.
 *
 * <p> Image cells are places where images can be dragged from and dropped onto.
 * Therefore, this class implements both the IDragSource and IDropTarget
 * interfaces.
 *
 */
public class ImageCell extends ImageView
        implements IDragSource, IDropTarget {

    public GridView mGrid;
    private TtbField field;
    
    public ImageCell(Context context, TtbField field) {
        super(context);
        this.field = field;
    }
    
    public void changePiece() {
        if (field.getOccupiedBy().getOwner().getColor().equalsIgnoreCase("red")) {
            this.setImageResource(R.drawable.numskull_red);
        }
        else {
            this.setImageResource(R.drawable.numskull_blue);
        }
    }

    // IDragSource interface methods
    /**
     * This method is called to determine if the IDragSource has something to
     * drag.
     *
     * @return True if there is something to drag
     */
    public boolean allowDrag() {
        // There is something to drag if the cell is not empty.
        return !this.isEmpty();
    }

    /**
     * This method is called on the completion of the drag operation so the IDragSource knows 
     * whether it succeeded or failed.
     * 
     * @param target View - the view that accepted the dragged object
     * @param success boolean - true means that the object was dropped successfully
     */
    public void onDropCompleted(View target, boolean success) {
        // If the drop succeeds, the image has moved elsewhere. 
        // So clear the image cell.
        if (success) {
            setImageResource(R.drawable.empty);
        }
    }
    public void onDragStart(DragView dragView, Object dragInfo) {

    }
    
    public void onDrag() {

    }

    //end of IDragSource interface implementation
    
// IDropTarget interface implementation
    /**
     * Handle an object being dropped on the IDropTarget. This is the where the
     * drawable of the dragged view gets copied into the ImageCell.
     *
     * @param source IDragSource where the drag started
     * @param x X coordinate of the drop location
     * @param y Y coordinate of the drop location
     * @param xOffset Horizontal offset with the object being dragged where the
     * original touch happened
     * @param yOffset Vertical offset with the object being dragged where the
     * original touch happened
     * @param dragView The DragView that's being dragged around on screen.
     * @param dragInfo Data associated with the object being dragged
     *
     */
    public void onDrop(IDragSource source, int x, int y, int xOffset, int yOffset,
            DragView dragView, Object dragInfo) {
        // The view being dragged does not actually change its parent and switch over to the ImageCell.
        // What we do is copy the drawable from the source view.
        ImageView sourceView = (ImageView) source;
        Drawable d = sourceView.getDrawable();
        if (d != null) {
            this.setImageDrawable(d);
        }
    }

    /**
     * React to a dragged object entering the area of this DropSpot. Provide the
     * user with some visual feedback.
     */
    public void onDragEnter(IDragSource source, int x, int y, int xOffset, int yOffset,
            DragView dragView, Object dragInfo) {
        //do nothing
        
    }

    /**
     * React to something being dragged over the drop target.
     */
    public void onDragOver(IDragSource source, int x, int y, int xOffset, int yOffset,
            DragView dragView, Object dragInfo) {
        //do nothing
    }

    /**
     * React to a drag
     */
    public void onDragExit(IDragSource source, int x, int y, int xOffset, int yOffset,
            DragView dragView, Object dragInfo) {
        //do nothing
    }

    /**
     * Check if a drop action can occur at, or near, the requested location.
     * This may be called repeatedly during a drag, so any calls should return
     * quickly.
     *
     * @param source IDragSource where the drag started
     * @param x X coordinate of the drop location
     * @param y Y coordinate of the drop location
     * @param xOffset Horizontal offset with the object being dragged where the
     * original touch happened
     * @param yOffset Vertical offset with the object being dragged where the
     * original touch happened
     * @param dragView The DragView that's being dragged around on screen.
     * @param dragInfo Data associated with the object being dragged
     * @return True if the drop will be accepted, false otherwise.
     */
    public boolean acceptDrop(IDragSource source, int x, int y, int xOffset, int yOffset,
            DragView dragView, Object dragInfo) {
        //always accept
        return true;
    }


    /**
     * Return true if this cell is empty. If it is, it means that it will accept
     * dropped views. It also means that there is nothing to drag.
     *
     * @return boolean
     */
    public boolean isEmpty() {
        return this.field.getOccupiedBy() == null;
    }

    /**
     * Call this view's onClick listener. Return true if it was called. Clicks
     * are ignored if the cell is empty.
     *
     * @return boolean
     */
    @Override
    public boolean performClick() {
        if (!isEmpty()) {
            return super.performClick();
        }
        return false;
    }

    /**
     * Call this view's onLongClick listener. Return true if it was called.
     * Clicks are ignored if the cell is empty.
     *
     * @return boolean
     */
    @Override
    public boolean performLongClick() {
        if (!isEmpty()) {
            return super.performLongClick();
        }
        return false;
    }

    /**
     * Returns the TthField object
     * @return 
     */
    public TtbField getField() {
        return this.field;
    }

    public void onAiDrag() {
        this.setImageResource(R.drawable.empty);
    }

    public void onAiDrop(IDragSource source) {
        ImageView sourceView = (ImageView) source;
        Drawable d = sourceView.getDrawable();
        if (d != null) {
            this.setImageDrawable(d);
        }
    }
} // end ImageCell
