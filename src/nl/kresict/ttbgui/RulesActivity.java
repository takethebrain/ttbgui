package nl.kresict.ttbgui;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class RulesActivity extends Activity {
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rules_layout);
        WebView mWebView = (WebView) findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("file:///android_asset/rulez.html");
    }
}
