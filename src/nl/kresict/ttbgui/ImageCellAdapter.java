package nl.kresict.ttbgui;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import nl.kresict.games.ttb.domain.Piece;
import nl.kresict.games.ttb.domain.TtbField;
import nl.kresict.games.ttb.domain.TtbGameBoard;
import nl.kresict.games.ttb.domain.TtbPiece;

/**
 * This class is used with a GridView object. It provides a set of ImageCell
 * objects that support dragging and dropping.
 *
 */
public class ImageCellAdapter extends BaseAdapter {

    // Variables
    public ViewGroup mParentView = null;
    private Context mContext;
    public Integer[] mThumbIds;
    public TtbField[] mFields;

    public ImageCellAdapter(Context c, TtbGameBoard board) {
        mContext = c;
        mThumbIds = new Integer[7 * 8];
        mFields = new TtbField[7 * 8];
        int position = 0;
        for (int x = 0; x <= TtbGameBoard.idxRows; x++) {
            for (int y = 0; y <= TtbGameBoard.idxColumns; y++) {
                TtbField field = board.getField(x, y);
                mFields[position] = field;
                TtbPiece piece = field.getOccupiedBy();
                if (piece != null) {
                    if (piece.getPiece().equals(Piece.BRAIN)) {
                        if (piece.getOwner().getColor().equalsIgnoreCase("red")) {
                            mThumbIds[position] = R.drawable.brain_red;
                        } else {
                            mThumbIds[position] = R.drawable.brain_blue;
                        }
                    } else if (piece.getPiece().equals(Piece.NINNY)) {
                        if (piece.getOwner().getColor().equalsIgnoreCase("red")) {
                            mThumbIds[position] = R.drawable.ninny_red;
                        } else {
                            mThumbIds[position] = R.drawable.ninny_blue;
                        }
                    } else if (piece.getPiece().equals(Piece.NUMSCULL)) {
                        if (piece.getOwner().getColor().equalsIgnoreCase("red")) {
                            mThumbIds[position] = R.drawable.numskull_red;
                        } else {
                            mThumbIds[position] = R.drawable.numskull_blue;
                        }
                    }
                } else {
                    mThumbIds[position] = R.drawable.empty;
                }
                position++;
            }
        }
    }

// Methods

    /**
     * getCount
     */
    public int getCount() {
        Resources res = mContext.getResources();
        int numImages = res.getInteger(R.integer.num_images);
        return numImages;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    /**
     * getView Return a view object for the grid.
     *
     * @return ImageCell
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        mParentView = parent;

        ImageCell v;
        if (convertView == null) {
            // If it's not recycled, create a new ImageCell.
            v = new ImageCell(mContext, mFields[position]);
            v.setImageResource(mThumbIds[position]);
            v.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        } 
        else {
            v = (ImageCell) convertView;
        }

        v.mGrid = (GridView) mParentView;
        
        v.setBackgroundResource(R.drawable.empty_field);

        // Set up to relay events to the activity.
        // The activity decides which events trigger drag operations.
        // Activities like the Android Launcher require a long click to get a drag operation started.
        v.setOnTouchListener((View.OnTouchListener) mContext);

        return v;
    }

} // end class
